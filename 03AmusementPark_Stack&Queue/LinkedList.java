/**
 * A class for Linked List which has a head and a tail
 * @author Chao
 *
 * @param <T>
 */

public class LinkedList<T> implements LinkedListInterface<T> {
    
    private Node head;
    private Node tail;
    

    /**
     * Add a new node to the front of your linked list 
     * that holds the given data.
     * 
     * @param t The data that the new node should hold.
     */
	public void addToFront(T t) {
	    Node<T> newNode = new Node<>();
	    newNode.data = t;
		newNode.next = head;
		head = newNode;
        if (isEmpty()) {
         tail = newNode;   
        }
	}

    /**
     * Add a new node to the back of your linked list
     * that holds the given data.
     * 
     * @param t The data that the new node should hold.
     */
	public void addToBack(T t) {
	    Node<T> newNode = new Node<>();
	    newNode.data = t;
	    newNode.next = null;
	    if (isEmpty()) {
	        tail = newNode;
	        head = newNode;
	    } else {
        tail.next = newNode;
        tail = newNode;
	    }
	}

    /**
     * Remove the front node from the list and return the
     * data from it.
     * 
     * @return The data from the front node or null.
     */
	public T removeFromFront() {
	    if (isEmpty()) {
	        return null;
        }
		T result = (T) head.data;
		head = head.next;
		return result;
	}

    /**
     * Remove the back node from the list and return the 
     * data from it.
     * 
     * @return The data from the last node or null.
     */
	public T removeFromBack() {
        if (isEmpty()) {
            return null;
        }
        T result = (T) tail.data;
        tail = null;
        return result;
	}

    /**
     * Return the linked list represented as a list of objects.
     * 
     * @return A copy of the linked list data as a list.
     */
	public T[] toList() {
        if (isEmpty()) {
            return null;
        }
        int length = size();
        T[] list = (T[]) new Object[length];
        int index = 0;
        for (Node tempNode = head; tempNode != null; tempNode = tempNode.next) {
            list[index] = (T) tempNode.data;
            index++;
        }
        return list;
	}

    /**
     * Return a boolean value representing whether or not
     * the list is empty.
     * 
     * @return True if empty. False otherwise.
     */
	public boolean isEmpty() {
	    return (head == null);
	}

    /**
     * Return the size of the list as an integer.
     * 
     * @return The size of the list.
     */
	public int size() {
        if (isEmpty()) {
            return 0;
        }
        int index = 0;
        for (Node tempNode = head; tempNode != null; tempNode = tempNode.next) {
            index++;
        }
        return index;
	}

    /**
     * Clear the list.
     */
	public void clear() {
		head = null;
		
	}

	/**
	 * The node class. Remember that this is a singularly linked list.
	 * 
	 * @author Steven Wojcio
	 */
	private class Node<T> {
		private T data;
		private Node next;
		
		/**
		 * default constructor
		 */
		public Node() {
            this(null, null);
        }
		/**
		 * constructor
		 * @param t
		 * @param next
		 */
		public Node(T t, Node next) {
            data = t;
            this.next = next;
        }
		
	      /**
         * constructor
         * @param t
         * @param next
         */
	     public Node(T t) {
	            data = t;
	            this.next = null;
	     }
		
	    /**
	     * get data
	     * @return
	     */
		public T getData() {
            return data;
        }
		
		/**
		 * set data
		 * @param data
		 */
		public void setData(T data) {
            this.data = data;
        }
		
		/**
		 * get next node
		 * @return
		 */
		public Node getNext() {
            return next;
        }
		
		/**
		 * set next node
		 * @param next
		 */
		public void setNext(Node next) {
            this.next = next;
        }
	}

}
