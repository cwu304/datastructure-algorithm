/**
 * Class of queue that follows the "FIFO, First In First Out" 
 * 
 * @author Chao
 *
 */
public class Queue<T> implements QueueInterface<T> {

	private LinkedListInterface<T> queue = new LinkedList<T>();

    /**
     * Add a node with the given data to the back of your queue.
     * 
     * @param t The data to add.
     */
	public void enqueue(T t) {
		queue.addToBack(t);
		
	}

    /**
     * Dequeue from the front of your queue.
     * 
     * @return The data from the front node or null.
     */
	public T dequeue() {
		return queue.removeFromFront();
	}

    /**
     * Return the size of the stack.
     * 
     * @return int size
     */
	public int size() {
		return queue.size();
	}

    /**
     * Return true if empty. False otherwise.
     * 
     * @return boolean result
     */
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	
}
