/**
 * A class for Park ticket selling system
 * @author Chao
 *
 */
import java.util.ArrayList;
import java.util.Scanner;

public class AmusementPark {
	
	//Leave this variable alone. It is necessary for compilation.
	public static int ticketNumber = 1000;
	
	public static void main(String[] args) {
	    
	    Stack<Ticket> pile = new Stack<Ticket>();
	    Queue<Patron> line = new Queue<Patron>();
	    ArrayList<Patron> lineTicket = new ArrayList<>();
	    int choice;
	    String name;
	
        Scanner keyboard = new Scanner(System.in);	    
	       do {
	            /**
	             * print out the menu
	             */
	            System.out.println("Welcome to the amusement park menu! What would you like to do?");
	            System.out.println("1: Add patron to the back of the line");
	            System.out.println("2: Put a ticket in the pile");
	            System.out.println("3: Get the number of patrons in line");
	            System.out.println("4: Get the number of available tickets");
	            System.out.println("5: Administer a ticket");
	            System.out.println("6: Show patrons with tickets");
	            System.out.println("7: Quit");
	            System.out.print("Enter your choice (1-7) here:");
	            choice = keyboard.nextInt();
	            
	            if (choice == 1) {
	                System.out.print("Enter the name of the patron:");
	                keyboard.nextLine(); //gets rid of the newline
	                name = keyboard.nextLine();
	                Patron newPatron = new Patron(name);
	                line.enqueue(newPatron);
	       
	            }
	            
	            if (choice == 2) {
	                Ticket newTicket = new Ticket();
	                pile.push(newTicket);
	                System.out.println("Done!");
	            }
	            
	            if (choice == 3) {
	                System.out.println("The number of patrons in line is: " + line.size());
                    
                }
	            
	            if (choice == 4) {
	                System.out.println("The number of available ticket is:" + pile.size()); 
                }
	            
	            if (choice == 5) {
                    if (line.isEmpty()) {
                        System.out.println("Nobody in line!");
                    }
                    if (pile.isEmpty()) {
                        System.out.println("Out of tickets!");
                    }
                    if ((!line.isEmpty()) && (!pile.isEmpty())) {
                        Ticket newTicket = pile.pop();
                        Patron newPatron = line.dequeue();
                        newPatron.giveTicket(newTicket);
                        lineTicket.add(newPatron);
                        System.out.println("Done!");
                    }
                }
	            
	            if (choice == 6) {
	                if (lineTicket.isEmpty()) {
                        System.out.println("No patron with ticket!");
                    } else {
                       for (Patron element : lineTicket) {
                        System.out.println(element.toString());
                       }  
                    }
                }
	       } while (choice != 7);
	}
}
