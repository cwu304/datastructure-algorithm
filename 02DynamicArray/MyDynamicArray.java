/**
 * A class of array that can resize dynamically
 * @author Chao
 * @version 1/23/2014
 */

import java.util.Collection;


public class MyDynamicArray<T> implements DynamicArrayInterface<T> {
	
	//Add instance variables here, they must be private
	private T[] bag;
	private static final int DEFAULT_INNITIAL_CAPACITY = 10;
	private int numberOfEntries;
	
	/**
	 * Defautl constructor
	 * Creates an empty bag whose initial capacity is 25.
	 */
	public MyDynamicArray() {
		this(DEFAULT_INNITIAL_CAPACITY);
	}
	
	
    /** 
     * Creates an empty bag having a given initial capacity.
     * @param capacity  the integer capacity desired 
     */
	public MyDynamicArray(int capacity) {
		numberOfEntries = 0;
        @SuppressWarnings("unchecked")
        T[] tempBag = (T[]) new Object[capacity]; // unchecked cast
        bag = tempBag;
			
	}
	
	/** 
     * Creates a bag containing given entries.
     * @param contents  an array of objects 
     */
    public MyDynamicArray(T[] contents) {
    	
        numberOfEntries = contents.length;
        @SuppressWarnings("unchecked")
        T[] tempBag = (T[]) new Object[numberOfEntries]; // unchecked cast
        bag = tempBag;
        for (int i = 0; i < contents.length; i++) {
			bag[i] = contents[i];
		}

    }
	
	/**
	 * Appends an item to the array
	 * @param toAdd the item to be added
	 */
	public void add(T toAdd) {
		ensureCapacity();
		bag[numberOfEntries] = toAdd;
		numberOfEntries++;
		
	}

	/**
	 * Append all the items of the collection to the array
	 * @param collection
	 */
	public void addAll(Collection<T> collection) {
		T[] addList = (T[]) collection.toArray();
		for (int i = 0; i < collection.size(); i++) {
			add(addList[i]);
		}	
	}

	/**
	 * Remove the given item (first occurrence) from the array
	 * Return the item removed, not the one passed in
	 * Move all items ahead of the one removed back one space
	 * 
	 * @param toRemove item to be removed
	 * @return the item removed
	 */
	public T remove(T toRemove) {
		T result = null;
		int index = 0;
		boolean found = false;
		for (int i = 0; i < numberOfEntries && (!found); i++) {
			if (bag[i].equals(toRemove)) {
				result = bag[i];
				found = true;
				index = i;
			}
		}
		
		if (!found) {
			System.out .println("No such item!");
			result = null;
		} else {
			
			if (index == numberOfEntries - 1) {
				bag[numberOfEntries] = null;
				numberOfEntries = numberOfEntries - 1;
			} else {
				for (int i = index; i < numberOfEntries - 1; i++) {
					bag[i] = bag[i + 1];
				}
				bag[numberOfEntries] = null;
				numberOfEntries = numberOfEntries - 1;
			}
		}
		
		return result;
	}

	/**
	 * Remove the item at the given index
	 * Move all items ahead of the one removed back one space
	 * Again, check for validity of the index
	 * @param index
	 * @return item removed, otherwise null
	 */
	public T remove(int index) {
		if (index < 0 || index >= numberOfEntries) {
			System.out.println("Not valid index!");
			return null;
		}
		
		if (index == numberOfEntries - 1) {
			bag[numberOfEntries] = null;
			numberOfEntries = numberOfEntries - 1;
		} else {
			for (int i = index; i < numberOfEntries - 1; i++) {
				bag[i] = bag[i + 1];
			}
			bag[numberOfEntries] = null;
			numberOfEntries = numberOfEntries - 1;
		}	
		return bag[index];
	}

	/**
	 * Gets the item at the given index
	 * Again, check for validity of the index
	 * @param index
	 * @return the item at the index, null otherwise
	 */
	public T get(int index) {
		if (index < 0 || index >= numberOfEntries) {
			System.out.println("Not valid index!");
			return null;
		}
		
		return bag[index];
	}

	/**
	 * Checks whether the array contains the given object
	 * @param obj the object that we are finding
	 * @return true if object is in the array, false otherwise
	 */
	public boolean contains(T obj) {
		boolean found = false;
		for (int i = 0; i < numberOfEntries && (!found); i++) {
			if (bag[i].equals(obj)) {
				found = true;
			}
		}
		return found;
	}
	
	/**
	 * Return the backing array
	 * This is for testing purposes only, normally we would 
	 * not want the user to have access to the backing structure
	 * @return The backing array itself, not a copy
	 */
	public T[] toArray() {
		return bag;
	}
     
	/**
	 * Returns whether the array is empty
	 * 
	 * @return whether the array is empty
	 */
	public boolean isEmpty() {
		// return true if it is empty
		return (numberOfEntries == 0); 
	}

	/**
	 * Clears the array, resets it to the original state
	 */
	public void clear() {
		numberOfEntries = 0;
        @SuppressWarnings("unchecked")
        T[] tempBag = (T[]) new Object[DEFAULT_INNITIAL_CAPACITY]; // unchecked cast
        bag = tempBag;
	}

	/**
	 * The number of items in the array
	 * 
	 * @return the number of items in the array
	 */
	public int size() {
		return numberOfEntries;
	}
	
    /** 
     * Doubles the size of the array bag if it is full
     */
    private void ensureCapacity() {
        if (numberOfEntries == bag.length) {
        	T[] newbag = (T[]) new Object[2 * bag.length];
            for (int i = 0; i < bag.length; i++) {
				newbag[i] = bag[i];
			}
        }
    }

}
