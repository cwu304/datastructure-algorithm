/**
 * A class represents a Ballot Box
 * @author Chao
 *
 */
public class BallotBox {
	private BagInterface<Ballot> ballots;
	
	/**
	 * Default constructor
	 */
	public BallotBox(){
		ballots = new ArrayListBag<Ballot>();
	}
	
	/**
	 * vote for a candidate
	 * @param name
	 * @param bribe
	 */
	public void vote(String name, double bribe){
		Ballot newEntry = new Ballot(name,bribe);
		ballots.add(newEntry);
		System.out.println("Vote for "+name+" Bribe: "+bribe);
	}
	
	/**
	 *  count the number of votes for a candidate
	 * @param name Candidate's name
	 * @return number of the votes
	 */
	public int countNumber(String name){
		int count;
		Ballot anEntry = new Ballot(name, 0.0);
		count = ballots.getFrequencyOf(anEntry);
		return count;
	}
	
	/**
	 * print names in the Ballotbox
	 */
	public void printNames() {
		Object[] array = ballots.toArray();
		for (Object curr:array){
			System.out.println(((Ballot)curr).getName()+"");
		}
	}
	
	
	/**
	 * remove a vote randomly
	 */
	public void remove() {
		if(!ballots.isEmpty()){
			ballots.remove();
			System.out.println("remove randomly!");
		}else{
			System.out.println("No votes in Box");
		}
	}
	
	/**
	 * remove a vote for a candidate;
	 * @param name
	 */
	public void remove(String name){
		Ballot anEntry = new Ballot(name, 0.0);
		Ballot temp = new Ballot();
		Object[] array = ballots.toArray();
		int size = array.length;
		if(ballots.contains(anEntry)){
			int index=-1;
			do{
				index++;
				if(anEntry.equals( (Ballot)array[index] )){
					temp  = (Ballot)array[index];
					}
				}while(!anEntry.equals( (Ballot)array[index]) && index<size);
				ballots.remove(temp);
				System.out.println("remove a vote for "+name);
			}else{
				System.out.println("No this candidate!");
		}
	}
	
	/**
	 * get the number of votes
	 * @param name
	 * @return
	 */
	public int getNumberOf(){
		return ballots.getCurrentSize();
	}
	
	/**
	 * empty the box
	 */
	public void empty() {
		ballots.clear();
	}
	
	public void printVotes() {
		if(ballots.isEmpty()){
			System.out.println("No vote!");
		}
		Object[] array = ballots.toArray();
		for (Object curr:array){
			System.out.println(((Ballot)curr).getName()+"/"+((Ballot)curr).getBribe());
		}
	}

}
