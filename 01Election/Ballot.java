/**
 * A class stores name and bribe amount
 * @author Chao
 *
 */
public class Ballot {
	private String name;
	private double bribe;
	
	
	public Ballot(){
		this(null,0.0);
	}
	/**
	 * construct
	 * @param name
	 * @param bribe
	 */
	public Ballot(String name, double bribe){
		this.name = name;
		this.bribe = bribe;
	}
	
	
	public String getName(){
		return name;
	}
	
	public double getBribe() {
		return bribe;
	}
	
	/**
	 * Two ballots are equal with same names
	 * @param ballot2 another ballot to campare
	 * @return true if they have the same names
	 */
	public boolean equals(Object other){ //getName getClass 
		
		if (this == other){
			return true;
		}
		if (other == null || (this.getClass() != other.getClass()) ){
			return false;
		}
		
		Ballot ballot2 = (Ballot)other;
		if (name.equals( ballot2.name )){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * @param
	 * @return the name and bribe
	 */
	public String toString(){ 
		return name+"/"+bribe;
	}
	
}
