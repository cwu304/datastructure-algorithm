/**
 * A class of bags based on Java class ArrayList
 * @author Chao 
 *
 */

import java.util.ArrayList;
import java.util.Random;

public class ArrayListBag<T> implements BagInterface<T>{
	private ArrayList<T> bags; 
	//private int numberOfEntries;
	
	public ArrayListBag(){
		bags = new ArrayList<>(); 
	}

	/** 
     * Gets the current number of entries in this bag.
     * 
     * @return the integer number of entries currently in the bag 
     */
    public int getCurrentSize(){
    	if(bags.isEmpty() ){
    		System.out.println("Empty!");
    	}
    	return bags.size();
    	
    }
    
    /** 
     * Sees whether this bag is empty.
     * 
     * @return true if the bag is empty, or false if not 
     */
    public boolean isEmpty(){
    	boolean empty = false;
    	if ( bags.isEmpty()){
    		empty = true;
    	}
    	return empty;
    }
    
    /** 
     * Adds a new entry to this bag.
     * If the new entry is null it should not be added.
     * 
     * @param newEntry  the object to be added as a new entry
     * @return true if the addition is successful, or false if not 
     */
    public boolean add(T newEntry){
    	return bags.add(newEntry);
    }
    
    /** 
     * Removes one unspecified entry from this bag, if possible.
     * Entry must be randomly chosen. (NOTE: the book's code
     * in the ArrayBag implementation did not chose randomly. 
     * This method must choose the entry randomly.)
     * 
     * @return the removed entry if the removal was successful, or null otherwise
     */
    public T remove(){
    	Random rand = new Random();
    	int index= rand.nextInt(bags.size());
    	return bags.remove(index);
    }
    
    /** 
     * Removes one occurrence of a given entry from this bag.
     * 
     * @param anEntry  the entry to be removed
     * @return true if the removal was successful, or false otherwise
     */
    public boolean remove(T anEntry){
    	return bags.remove(anEntry);
    }
   
    /** 
     * Removes all entries from this bag.
     */
    public void clear(){
    	bags.clear();
    }
    
    /** 
     * Counts the number of times a given entry appears in this bag.
     * 
     * @param anEntry  the entry to be counted
     * @return the number of times anEntry appears in the bag 
     */
    public int getFrequencyOf(T anEntry){
    	int counter = 0;
    	for (int index = 0; index < bags.size();index++){
    		boolean eq = anEntry.equals( bags.get(index));
    		if(anEntry.equals( bags.get(index))){
    			counter++;
    		}
    	}
    	return counter;
    }
    
    /** 
     * Tests whether this bag contains a given entry.
     * 
     * @param anEntry  the entry to locate
     * @return true if this bag contains anEntry, or false otherwise 
     */
    public boolean contains(T anEntry){
    	return bags.contains(anEntry );
    }
    
    /** 
     * Creates an array of all entries that are in this bag.
     * It does not destroy or alter the bag in any way.
     * If the bag is actually empty, this will still create an
     * array, but it will have length 0.
     * 
     * @return a newly allocated array of all the entries in the bag 
     */
    public T[] toArray(){
    	int size = bags.size();
    	T[] array = (T[]) new Object[size];
    	for(int index =0; index<bags.size();index++){
    		array[index]= bags.get(index);	
    	}
    	return array;
    }

}