/**
 * A class presents the election, contains menu
 * @author Chao
 * @version 1.0 1/17/2014
 */
import java.util.Scanner;

public class Election {
	public static void main(String args[]){
		
		BallotBox aBox = new BallotBox();
		Scanner keyboard = new Scanner(System.in);
		int choice;
		String name;
		double bribe;
		
		do {
			/**
			 * print out the menu
			 */
			System.out.println("Welcome to the election! What would you like to do?");
			System.out.println("1: Vote for a candidate");
			System.out.println("2: Count the number of votes for a candidate");
			System.out.println("3: Remove a vote");
			System.out.println("4: Get number of votes in the ballot box");
			System.out.println("5: Empty the ballot box");
			System.out.println("6: Print all votes");
			System.out.println("7: Quit");
			System.out.print("Enter your choice (1-7) here:");
			choice = keyboard.nextInt();
			
			if(choice == 1){
				System.out.print("Enter the name of the candidate you would like to vote for:");
				keyboard.nextLine(); //gets rid of the newline
				name = keyboard.nextLine();
				System.out.print("Enter bribe amount:");
				bribe= keyboard.nextDouble();
				aBox.vote(name, bribe);
				//System.out.println(name);
				//System.out.println(bribe);
			}
			
			if(choice == 2){
				System.out.print("Enter the name of the candidate you would like to count:");
				keyboard.nextLine(); //gets rid of the newline
				name = keyboard.nextLine();
				System.out.println(name);
				//int count = aBox.countNumber(name);
				System.out.println("Candidate "+name+" got: "+aBox.countNumber(name)+" votes!");
			}
			if(choice == 3){
				System.out.println("Remove a vote");
				System.out.println("1. Choose a vote to remove. ");
				System.out.println("2. Remove randomly!");
				System.out.print("Enter your choice (1-2) here:");
				int choice2 = keyboard.nextInt();
				if(choice2 == 1){
					aBox.printNames();
					System.out.print("Enter the name of the candidate you would like to remove:");
					keyboard.nextLine(); //gets rid of the newline
					name = keyboard.nextLine();
					aBox.remove(name);
				}
				if(choice2 == 2){
					aBox.remove();
				}
			}
			if(choice == 4){
				System.out.println("The number of votes in the ballot box is "+aBox.getNumberOf());
			}
			if(choice == 5){
				aBox.empty();
				System.out.println("It's Empty!");
			}
			if(choice == 6){
				aBox.printVotes();
			}
			if(choice == 7){			
				System.out.println("See you!");
			}
		} while (choice!=7);

		keyboard.close();
	}

}
